import React, { Component } from 'react';
import { Col , Row} from 'react-bootstrap';
import { Paper, Avatar,  RaisedButton} from 'material-ui';
import {white, cyan500} from 'material-ui/styles/colors';
import './App.css';

class App extends Component {
  constructor() {
    super();
  this.state={
    team:[
      {name: 'Amazon', url : 'amazon.com', notifications: 15 },
      {name: 'Gmail', url : 'gmail.com', notifications: 0 },
      {name: 'Ebay', url : 'ebay.com', notifications: 5 },
    ]

  }
}
addTeam() {
  console.log(this); // Code to add more teams.
}
render() {
  return (
    <div className="customCard">
        <Header />
        <b className="subHeader"> Your Teams </b>
        {this.state.team.map((name, i) => <TableRow key = {i}
               team = {name} />)}
        <AddButton />
        <Button />
    </div>
  );
}
}
const Header = () => {
    return (
          <Col xs={12}>
            <Col xs={9}>
              <h1 className="header">WHICH TEAM DO YOU WANT TO LOG IN?</h1>
            </Col>
          </Col>
    );
}

const TableRow = (props) => {
    return (
      <Col xs={12} className="teamCard" >
        <Col xs={2} >
            <CustomAvatar name={props.team.name}/>
        </Col>
         <Col xs={4} className="alignRow">
          <Row>
            <b>{props.team.name} </b>
          </Row>
          <Row>{props.team.url}</Row>
         </Col>
         <Notifications notifications={props.team.notifications}/>
       </Col>
     );
}

const Notifications = (props) => {
  if(props.notifications > 0){
    return (<Col xs={5} >
              <p className="notificationColor alignRow">{props.notifications} Notifications!</p>
            </Col>
          )
  }
  return (<Col xs={5}>
          </Col>)
}
const style ={
  width: 60,
  heigth: 60,
  padding: 10,
  textAlign:'center',
  cursor: 'pointer'
};
const CustomAvatar = (props) => {
  return (
    <Paper style={style} zDepth={1}>
      <Avatar>{props.name.substring(0,1)}</Avatar>
    </Paper>
  )
}
const Button = () => {
  return (
    <Row >
      <Col xs={4} xsOffset={4}>
        <RaisedButton className="signButton" label="Sign Out" />
      </Col>
    </Row>
  )
}

const AddButton = () => {
  return (
    <Col xs={12} className="teamCard" >
      <Col xs={2} >
        <Paper style={style} zDepth={1}> <Avatar color={cyan500} size={40} backgroundColor={white}> + </Avatar> </Paper>
        </Col>
        <Col xs={4} className="addButton cursorClickable">
         <Row>
          <b> Create New Team </b>
         </Row>
        </Col>
    </Col>
  )
}

export default App;
