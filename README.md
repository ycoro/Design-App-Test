Run:
  $ npm install \n
  $ npm start

  Open browser on port: 3000

  Note: You need node: >=6 and React installed on your system.
